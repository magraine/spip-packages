<?php

namespace Spip\Tools\Packages;

use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputInterface;


class Application extends ConsoleApplication {

	const NAME = "Spip Packages";
	const VERSION = "1.1.0";

	/**
	 * Application constructor.
	 *
	 * Permet d’appliquer une commande à un ensemble de sites
	 * d’une mutualisation.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = []) {
		parent::__construct(self::NAME, self::VERSION);

		$this->add(new Command\GeneratePackages());
		$this->add(new Command\GenerateSpipLoaderList());
	}
}