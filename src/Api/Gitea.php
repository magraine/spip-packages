<?php

namespace Spip\Tools\Packages\Api;

class Gitea extends ApiAbstract {


    public function getBranches(string $package) : ?array {
        $branches = $this->get($package, 'branches');
        return $this->simplify($package, $branches, 'branch');
    }

    public function getTags(string $package) : ?array {
        $tags = $this->get($package, 'tags');
        return $this->simplify($package, $tags, 'tag');
    }

    public function getUrlArchive(string $package, string $name) : string {
        return "{$this->url}$package/archive/$name.zip";
    }

    public function getFileContent(string $package, string $name, string $type, string $file): string
    {
        if ($type === 'tags') {
            $type = 'tag';
        } elseif ($type === 'branches') {
            $type = 'branch';
        }
        // https://git.spip.net/spip/dist/raw/tag/spip/3.2.7/paquet.xml
        $url = "{$this->url}$package/raw/$type/$name/$file";
        return $this->getUrlContent($url, $this->useCacheOnPackage($package));
    }

    private function get(string $package, string $endpoint) : ?array {
        $api = "{$this->url}api/v1/repos/$package/$endpoint";
        return $this->getJson($api, $this->useCacheOnPackage($package));
    }
}