<?php

namespace Spip\Tools\Packages\Api;

class Gitlab extends ApiAbstract {


    public function getBranches(string $package) : ?array {
        $branches = $this->get($package, 'branches');
        return $this->simplify($package, $branches, 'branch');
    }

    public function getTags(string $package) : ?array {
        $tags = $this->get($package, 'tags');
        return $this->simplify($package, $tags, 'tag');
    }

    // {url}/spip/spip/-/archive/master/spip-master.zip
    // {url}/spip/spip/-/archive/spip-3.2/spip-spip-3.2.zip
    // {url}/spip/spip/-/archive/spip-3.2.7/spip-spip-3.2.7.zip
    // {url}/spip/aide/-/archive/spip/3.2.7/aide-spip-3.2.7.zip
    public function getUrlArchive(string $package, string $name) : string {
        list($orga, $repo) = explode('/', $package, 2);
        $file = $repo . '-' . str_replace('/', '-', $name);
        return "{$this->url}$package/-/archive/$name/$file.zip";
    }

    public function getFileContent(string $package, string $name, string $type, string $file): string
    {
        // https://git-mirror.spip.net/spip/aide/-/raw/spip/3.2.7/paquet.xml
        $url = "{$this->url}$package/-/raw/$name/$file";
        return $this->getUrlContent($url, $this->useCacheOnPackage($package));
    }

    private function get(string $package, string $endpoint) : ?array {
        $_package = urlencode($package);
        $api = "{$this->url}api/v4/projects/$_package/repository/$endpoint?per_page=1000";
        return $this->getJson($api, $this->useCacheOnPackage($package));
    }
}