<?php

namespace Spip\Tools\Packages\Api;

use Psr\SimpleCache\CacheInterface;

abstract class ApiAbstract {
    protected $url;
    protected $cache;
    /** Ignore read cache on this packages */
    protected $ignores = [];

    public function __construct(string $url, CacheInterface $cache = null) {
        $this->url = $url;
        /** @var CacheInterface */
        $this->cache = $cache;
    }

    abstract public function getBranches(string $package) : ?array;
    abstract public function getTags(string $package) : ?array;
    abstract public function getUrlArchive(string $package, string $name) : string;
    abstract public function getFileContent(string $package, string $name, string $type, string $file) : string;

    public function getJson(string $url, bool $useCache = true) : ?array {
        $content = $this->getUrlContent($url, $useCache);
        $json = \json_decode($content, true);
        return $json;
    }

    public function getUrlContent(string $url, bool $useCache = true) : ?string {
        $key = \urlencode($url);
        if ($useCache) {
            $content = $this->cache->get($key);
            if ($content !== null) {
                return $content;
            }
        }
        $content = \file_get_contents($url);
        $this->cache->set($key, $content);
        return $content;
    }

    public function ignoreCacheOnPackage(string $package) : void {
        $this->ignores[] = $package;
    }

    public function useCacheOnPackage(string $package) : bool {
        return !in_array($package, $this->ignores);
    }

    protected function simplify(string $package, array $items, string $type) : array {
        $_items = [];
        foreach ($items as $item) {
            $name = $item["name"];
            $_items[$name] = [
                "url" => $this->getUrlArchive($package, $name),
                "original_name" => $name,
                "type" => $type,
            ];
        }
        return $_items;
    }
}