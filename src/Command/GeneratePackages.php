<?php

namespace Spip\Tools\Packages\Command;

use \Kodus\Cache\FileCache;
use Spip\Tools\Packages\Api\ApiAbstract;
use Spip\Tools\Packages\Api\Gitea;
use Spip\Tools\Packages\Api\Gitlab;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GeneratePackages extends Command {

	/** @var SymfonyStyle */
	private $io;

	private $gitea = 'https://git.spip.net/';
	private $gitlab = 'https://git-mirror.spip.net/';
	/** @var ApiAbstract */
	private $api;
	/** @var CacheInterface */
	private $cache;
	/** @var string */
	private $outputDir;

	private $requires = [
		'packages' => ['spip/security'],
		'spip-classic' => ['spip/spip'],
		'spip-plugin' => [
			"spip/aide",
			"spip/archiviste",
			"spip/bigup",
			"spip/breves",
			"spip/compagnon",
			"spip/compresseur",
			"spip/dist",
			"spip/dump",
			"spip/filtres_images",
			"spip/forum",
			"spip/jquery_ui",
			"spip/mediabox",
			"spip/medias",
			"spip/mots",
			"spip/organiseur",
			"spip/petitions",
			"spip/plan",
			"spip/porte_plume",
			"spip/revisions",
			"spip/safehtml",
			"spip/sites",
			"spip/squelettes_par_rubrique",
			"spip/statistiques",
			"spip/svp",
			"spip/textwheel",
			"spip/urls_etendues",
			"spip/vertebres"
		]
	];

	private $alias = [
		"spip/security" => "spip-contrib-outils/securite",
	];

	private $packages = [];

	private $spip_versions = [];

	protected function configure() {
		$this
			->setName('generate:packages')
			->setDescription('Génère un fichier packages.json')
			->addOption('api', null, InputOption::VALUE_REQUIRED, 'Api à utiliser : gitea | gitlab.', 'gitlab')
			->addOption('output-dir', null, InputOption::VALUE_REQUIRED, 'Répertoire où créer le fichier json. Défaut répertoire en cours')
			->addOption('cache-dir', null, InputOption::VALUE_REQUIRED, 'Répertoire du cache. Défaut ./tmp/cache')
			->addOption('ignore-cache-on-package', null, InputOption::VALUE_REQUIRED, 'Ignorer le cache pour une dépendance. Exemple «spip/compresseur»')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$io = $this->io = new SymfonyStyle($input, $output);
		$io->title("Générer packages.json");

		$this->init($input, $output);
		$this->generatePackages();
		$this->makeJson();
		
		return 0;
	}

	private function init(InputInterface $input, OutputInterface $output) {
		$outputDir = $input->getOption('output-dir');
		if ($outputDir) {
			if (!is_dir($outputDir)) {
				throw new \Exception("Directory '$outputDir' does not exists");
			}
			$this->outputDir = $outputDir;
		} else {
			$this->outputDir = getcwd();
		}

		$cacheDir = $input->getOption('cache-dir');
		if (!$cacheDir) {
			$cacheDir = getcwd() . '/tmp/cache';
		}
		if (!is_dir($cacheDir)) {
			mkdir($cacheDir, 0777, true);
		}

		$cache = new FileCache($cacheDir, 3600);
		$cache->cleanExpired();
		$this->cache = $cache;

		if ($input->getOption('api') === 'gitea') {
			$this->api = new Gitea($this->gitea, $cache);
		} else {
			$this->api = new Gitlab($this->gitlab, $cache);
		}
		$package = $input->getOption('ignore-cache-on-package');
		if ($package) {
			$this->api->ignoreCacheOnPackage($package);
		} 
	}

	private function generatePackages() {
		foreach ($this->requires as $type => $packages) {
			foreach ($packages as $package) {
				$this->addPackage($package, $type);
			}
		}
	}

	private function makeJson() {
		$data = ["packages" => $this->packages];
		$json = json_encode($data, JSON_PRETTY_PRINT|JSON_FORCE_OBJECT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
		file_put_contents($this->outputDir . DIRECTORY_SEPARATOR . "packages.json", $json);
	}

	private function addPackage(string $package, string $type = null) {
		$this->io->writeln("Traite <info>$package</info>");
		$desc = $this->describe($package);
		foreach ($desc['branches'] as $branch => $data) {
			$this->addPackageVersion($package, $branch, $data, $type);
		}
		foreach ($desc['tags'] as $tag => $data) {
			$this->addPackageVersion($package, $tag, $data, $type);
		}
	}

	private function addPackageVersion(string $package, string $version_name, array $data, string $type = null) {
		if (empty($this->packages[$package])) {
			$this->packages[$package] = [];
		}
		$json = [
			"name" => $package,
			"version" => $version_name,
			"dist" => [
				"url" => $data['url'],
				"type" => "zip",
			],
		];
		if ($type and $type !== 'package') {
			$json["type"] = $type;
		}
		$this->packages[$package][$version_name] = $json;
	}



	private function describe(string $package) : array {
		$_package = $this->alias[$package] ?? $package;
		$branches = $this->api->getBranches($_package, 'branches');
		$tags = $this->api->getTags($_package, 'tags');
		return [
			'branches' => $this->filter($package, $branches, 'branches'),
			'tags' => $this->filter($package, $tags, 'tags'),
		];
	}


	private function filter(string $package, array $items, string $type) : array {
		$isBranch = $type === 'branches';
		$isSPIP = in_array($package, $this->requires['spip-classic'] ?? []);
		if ($isSPIP) {
			$items = $this->filterSpip($package, $items, $type);
		}
		if ($items) {
			$tags = [];
			foreach ($items as $name => $desc) {
				// ignore list
				if (
					in_array($name[0], ['d', 'i', 'f'])
					and preg_match("/^(dev|issue|fix)[-_\/]/i", $name)
				) {
					unset($items[$name]);
					continue;
				}
				if (!$isBranch && !$isSPIP) {
					if ($name[0] === 's' and preg_match("/^spip[-_\/](.*)/i", $name, $m)) {
						$version = $m[1];
						unset($items[$name]);
						if (in_array($version, $this->spip_versions)) {
							$name = 'dev-' . str_replace('/', '-', $name);
							$items[$name] = $desc;
							$this->addSpipVersionRequire($version, $package, $name, $type);
						}
						continue;
					}
				}
				if ($isBranch) {
					$require = false;
					unset($items[$name]);
					if (preg_match("/^v?(\d+[.]\d+)/i", $name, $m)) {
						$branch = $m[1];
						$name = $branch . '.x-dev';
					} elseif (preg_match("/^spip[-_\/](\d+[.]\d+)/i", $name, $m)) {
						$branch = $m[1];
						$require = "dev-$name";
						$name = $branch . '.x-dev';
					} else {
						$branch = $name;
						$name = "dev-$branch";  
						if ($branch === "master" and !$isSPIP) {
							$require = $name;
						}
					}
					$items[$name] = $desc;
					if (!$isSPIP && $require) {
						if (in_array($branch, $this->spip_versions)) {
							$this->addSpipRequire($name, $package, $require);
						}
					}
				} elseif (!$isSPIP) {
					unset($items[$name]);
					$tags[$name] = $desc;
				}
			}
			if ($tags) {
				uksort($tags, 'version_compare');
				$tags = array_reverse($tags, true);
				$tags = array_slice($tags, 0, 2);
				$items += $tags;
			}
		}
		return $items;
	}


	private function filterSpip(string $package, array $items, string $type) : array {
		if ($items) {
			foreach ($items as $name => $url) {
				$version = $name;
				if ($name[0] === 'v') {
					$version = substr($name, 1);
				}
				if (in_array($version[0], [0,1,2,3,4,5,6,7,8,9])) {
					unset($items[$name]);
					if ($version[0] === "3" and $version[2] === "1") {
						if (version_compare($version, "3.1.11", ">=") or $version === "3.1") {
							$items[$version] = $url;
							$this->spip_versions[] = $version;
						}
					} elseif ($version[0] === "3" and $version[2] === "2") {
						if (version_compare($version, "3.2.6", ">=") or $version === "3.2") {
							$items[$version] = $url;
							$this->spip_versions[] = $version;
						}
					}
				}
				if ($name === 'master') {
					$items[$version] = $url;
					$this->spip_versions[] = $name;
				}
			}
		}
		return $items;
	}

	private function addSpipVersionRequire(string $version_spip, string $package, string $name, string $type) {
		$this->addSpipRequire($version_spip, $package, $name);
		#$paquet = $this->api->getFileContent($package, $name, $type, 'paquet.xml');
		#if (preg_match("/version\s?=\s?\"(.*)\"/", $paquet, $m)) {
		#	$version = $m[1];
		#	$this->addSpipRequire($version_spip, $package, $version);
		#}
	}

	private function addSpipRequire(string $version_spip, string $package, string $version) {
		if (!isset($this->packages["spip/spip"][$version_spip]["require"])) {
			$this->packages["spip/spip"][$version_spip]["require"] = [];
		}
		$this->packages["spip/spip"][$version_spip]["require"][$package] = $version;
	}
}
