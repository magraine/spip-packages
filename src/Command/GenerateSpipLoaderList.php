<?php

namespace Spip\Tools\Packages\Command;

use \Kodus\Cache\FileCache;
use Spip\Tools\Packages\Api\ApiAbstract;
use Spip\Tools\Packages\Api\Gitea;
use Spip\Tools\Packages\Api\Gitlab;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateSpipLoaderList extends Command {

	/** @var SymfonyStyle */
	private $io;

	private $branche_min = [3, 1]; // 3.1 mini
	private $fileName = "spip_loader_list.json";

	private $gitea = 'https://git.spip.net/';
	private $gitlab = 'https://git-mirror.spip.net/';
	/** @var ApiAbstract */
	private $api;
	/** @var CacheInterface */
	private $cache;
	/** @var string */
	private $outputDir;

	private $api_version = 1;
	private $versions = [];

	protected function configure() {
		$this
			->setName('generate:spiploaderlist')
			->setDescription('Génère un fichier spip_loader_list.json')
			->addOption('api', null, InputOption::VALUE_REQUIRED, 'Api à utiliser : gitea | gitlab.', 'gitlab')
			->addOption('output-dir', null, InputOption::VALUE_REQUIRED, 'Répertoire où créer le fichier json. Défaut répertoire en cours')
			->addOption('cache-dir', null, InputOption::VALUE_REQUIRED, 'Répertoire du cache. Défaut ./tmp/cache')
			->addOption('ignore-cache', null, InputOption::VALUE_NONE, 'Ignorer le cache d’api')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$io = $this->io = new SymfonyStyle($input, $output);
		$io->title("Générer spip_loader_list.json");

		$this->init($input, $output);
		$this->generateList();
		$this->makeJson();
		
		return 0;
	}

	private function init(InputInterface $input, OutputInterface $output) {
		$outputDir = $input->getOption('output-dir');
		if ($outputDir) {
			if (!is_dir($outputDir)) {
				throw new \Exception("Directory '$outputDir' does not exists");
			}
			$this->outputDir = $outputDir;
		} else {
			$this->outputDir = getcwd();
		}

		$cacheDir = $input->getOption('cache-dir');
		if (!$cacheDir) {
			$cacheDir = getcwd() . '/tmp/cache';
		}
		if (!is_dir($cacheDir)) {
			mkdir($cacheDir, 0777, true);
		}

		$cache = new FileCache($cacheDir, 3600);
		$cache->cleanExpired();
		$this->cache = $cache;

		if ($input->getOption('api') === 'gitea') {
			$this->api = new Gitea($this->gitea, $cache);
		} else {
			$this->api = new Gitlab($this->gitlab, $cache);
		}
		$package = $input->getOption('ignore-cache');
		if ($package) {
			$this->api->ignoreCacheOnPackage('spip/spip');
		} 
	}

	private function generateList() {
		$lastTags = $this->getLastTags();
		// "spip/archives/spip-v3.2.5.zip"
		// "spip/dev/spip-master.zip"
		$versions = [];
		$versions["dev"] = "spip/dev/spip-master.zip";
		foreach ($lastTags as $version => $tag) {
			$versions[$version] = "spip/archives/spip-v$version.zip";
		}
		$this->versions = $versions;
	}

	private function getLastTags() {
		$tags = $this->api->getTags('spip/spip');

		/*
			"v3.1.13" => array:3 [
				"url" => "https://git.spip.net/spip/spip/archive/v3.1.13.zip"
				"original_name" => "v3.1.13"
				"type" => "tag"
			]
			...
		*/
		
		$tags = array_filter($tags, function($tag) {
			$t = $tag['original_name'];
			if ($t[0] !== 'v') {
				return null;
			}
			$t = substr($t, 1);
			$v = explode('.', $t);
			if ($v[0] < $this->branche_min[0]) {
				return null;
			}
			if ($v[0] == $this->branche_min[0] and $v[1] < $this->branche_min[1]) {
				return null;
			}
			return $tag;
		});
		// ici on a les tags 3.1+ (branche_min +)
	
		// on regroupe par branche
		$groups = [];
		foreach ($tags as $t => $tag) {
			$branch = $this->getBranchNameFromTag($t);
			if (!isset($groups[$branch])) {
				$groups[$branch] = [];
			}
			$t = substr($t, 1);
			$groups[$branch][$t] = $tag;
		}
		
		// on conserve la dernière uniquement
		$list = [];
		foreach ($groups as $branch => $tags) {
			$versions = array_keys($tags);
			$max = 0;
			foreach ($versions as $v) {
				if (version_compare($v, $max, '>')) {
					$max = $v;
				}
			}
			$list[$max] = $tags[$max];
		}
		uksort($list, 'version_compare');
		$list = array_reverse($list, true);
		return $list;
	}

	private function getBranchNameFromTag($tag) {
		$v = substr($tag, 1);
		$b = explode('.', $v);
		$branch = [];
		$branch[] = array_shift($b);
		$branch[] = array_shift($b);
		$branch = implode('.', $branch); 
		return $branch;
	}

	private function makeJson() {
		$data = [
			"api" => $this->api_version,
			"versions" => $this->versions
		];
		$json = json_encode($data, JSON_PRETTY_PRINT|JSON_FORCE_OBJECT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
		file_put_contents($this->outputDir . DIRECTORY_SEPARATOR . $this->fileName, $json);
	}


}
