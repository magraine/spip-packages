# Spip Packages

Permet de générer certains fichiers en relation avec les releases SPIP :

- packages.json (pour le script d’archive utilisant Composer)
- spip_loader_list.json (pour Spip Loader)

Un cache d’1h est appliqué à chaque appel aux API (gitea ou gitlab).
Utilise par défaut d’api du mirroir gitlab.

## Alias

- `composer spip-packages` est un alias vers `php bin/spip-packages generate:packages`
- `composer spip-loader-list` est un alias vers `php bin/spip-packages generate:spiploaderlist`

## Génerer le packages.json

```sh
# Génère packages.json
composer spip-packages

# Utilise l’api gitea (plus lente)
composer spip-packages -- --api=gitea

# Génère le packages.json dans ce répertoire
composer spip-packages --  --output-dir=build

# Ignore le cache (et le recrée) de ce paquet
composer spip-packages --  --ignore-cache-on-package=spip/compresseur
```

## Générer le spip_loader_list.json

un Json des dernières versions de SPIP (pour SPIP Loader)

Un peu en dehors du contexte de composer, mais on utilise les mêmes API, du coup...

```sh
# Génère spip_loader_list.json
composer spip-loader-list

# Utilise l’api gitea (plus lente)
composer spip-loader-list -- --api=gitea

# Génère le spip_loader_list.json dans ce répertoire
composer spip-loader-list --  --output-dir=build

# Ignore le cache d’api (et le recrée)
composer spip-loader-list --  --ignore-cache
```
